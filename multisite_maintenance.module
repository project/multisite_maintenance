<?php

/**
 * @subpackage HOOKS
 */

/**
 * Implementation of hook_help().
 */
function multisite_maintenance_help($section) {
  switch ($section) {
    case 'admin/help#multisite_maintenance':
      return '<p>'. t('Allows batch maintenance of multisites including taking sites offline, ' .
          'and running update.php.') .'</p>';
    case 'admin/multisite/multisite_maintenance/site_status':
      return '<p>'. t('You can alter the status of any site below.  Only those sites whose databases ' .
          'can be reached can be changed (Other sites may be for use on another staging or production ' .
          'server).  Note that the cache will also be cleared for all sites that you take offline.') .'</p>';
    case 'admin/multisite/multisite_maintenance/update':
      return '<p>'. t('Choose the sites that you wish to run update.php for.  Only those sites whose ' .
          'databases can be reached can be taken offline (Other sites may be for use on another staging ' .
          'or production server).') .'</p>' .
          '<h3>'. t('Technique') .'</h3>'. 
          '<p>'. t('For each site to be updated, $db_url is changed on the fly, global variables are ' .
          'rebuilt, caches are cleared, the update functions are run (with the default "latest version" ' .
          'for each updated module), and then everything is restored to the original values.') .'</p>'. 
          '<h3>'. t('Challenges and Limitations') .'</h3>'.
          '<p>'. t('The following globals cannot be rebuilt: base_url (if not explicitly set in each ' .
          'settings.php file, therefore we recommend always explicitly setting this).  Also be aware ' .
          'that static variables cannot be reset (see conf_path() as an example).  If an update is relying ' .
          'on a static variable that would be different on each site the update will be completed in error ' .
          'but without any indication of such. It should be noted that this edge case should be very rare, ' .
          'in theory update.php will only be making changes to the database (perhaps modifying a module\'s ' .
          'tables and using variable_set).') .'</p>';
    case 'admin/multisite/multisite_maintenance':
      return '<p>'. t('Multisite Maintenance can make multisite Drupal installations easier to manage ' .
          'by providing tools to take selected sites off-line, and to update selected sites.  Note that ' .
          'by visiting the status and update tabs, the code in all of your settings.php files will be ' .
          'evaluated.  This should not be a problem unless you have some really unusual customization in ' .
          'your settings.php files.') .'</p>';  
  }
}

/**
 * Implementation of hook_perm().
 */
function multisite_maintenance_perm() {
  return array('perform maintenance on multisites');
}


/**
 * Implementation of hook_menu().
 */
function multisite_maintenance_menu($may_cache) {

  $items = array();
  $access = user_access('perform maintenance on multisites');

  if ($may_cache) {
    $items[] = array(
        'path'                => 'admin/multisite/multisite_maintenance',
        'title'               => t('Multisite maintenance'),
        'callback'            => 'drupal_get_form', 
        'callback arguments'  => array('multisite_maintenance_info_form'), 
        'access'              => $access,
        'type'                => MENU_NORMAL_ITEM, 
      );
      $items[] = array(
        'path'                => 'admin/multisite/multisite_maintenance/info',
        'title'               => t('Info'),
        'type'                => MENU_DEFAULT_LOCAL_TASK, 
      );
      $items[] = array(
        'path'                => 'admin/multisite/multisite_maintenance/site_status',
        'title'               => t('Site Status'),
        'callback'            => 'drupal_get_form', 
        'callback arguments'  => array('multisite_maintenance_status_form'), 
        'access'              => $access,
        'type'                => MENU_LOCAL_TASK, 
      );
      $items[] = array(
        'path'                => 'admin/multisite/multisite_maintenance/update',
        'title'               => t('Update Sites'),
        'callback'            => 'drupal_get_form', 
        'callback arguments'  => array('multisite_maintenance_update_form'), 
        'access'              => $access,
        'type'                => MENU_LOCAL_TASK, 
      );
  }

  return $items;

}


/**
 * @subpackage FORMS
 */
 
/**
 * multisite info page
 */
function multisite_maintenance_info_form() {

  // test for mysqldump
  $form['mysql']['#value'] = '<strong>'. t('mysqldump') .':</strong> ';
  if (_multisite_maintenance_test_shell_app('mysqldump')) {
    $form['mysql']['#value'] .= t('Found');
  }
  else {
    $form['mysql']['#value'] .= t('Not found. Your database(s) will not be backed up before you run the mutlisite update.  To rectify this, make sure that the path for mysqldump is included in your system\'s $PATH variable.');    
  }
  
  // test for gzip
  $form['gzip']['#value'] = '<br /><strong>'. t('gzip') .':</strong> ';
  if (_multisite_maintenance_test_shell_app('mysqldump')) {
    $form['gzip']['#value'] .= t('Found');
  }
  else {
    $form['gzip']['#value'] .= t('Not found. Your backups will not be compressed before you run the mutlisite update.  To rectify this, make sure that the path for gzip is included in your system\'s $PATH variable.');    
  }
  
  // backup dir
  $form['multisite_maintenance_backup_dir'] = array(
    '#type'           => 'textfield',
    '#title'          => 'Backup Directory',
    '#default_value'  => variable_get('multisite_maintenance_backup_dir', ''),
    '#description'    => 'Enter the absolute path to the directory that you wish to store your backups to.  It is recommended that you store backups outside of the webroot.  The directory that you choose must be writable by the apache user.',
  );
  
  // check for devel
  if (module_exists('devel') && variable_get('devel_error_handler', '1') == '2') {
    drupal_set_message('You have the devel module enabled with error backtrace.  You may see a series of mysql errors on the subsequent pages. Switching to the standard backtrace is recommended.');
  }
    
  return system_settings_form($form); 
}

function multisite_maintenance_info_form_submit($form_id, $form_values) {
  if (!file_check_directory($form_values['multisite_maintenance_backup_dir'], FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('The directory is not writable. Your database(s) will not be backed up before you run the mutlisite update.'), 'error');        
  } 
  else{
    variable_set('multisite_maintenance_backup_dir', $form_values['multisite_maintenance_backup_dir']);
  } 
}


/**
 * abstracted form
 */
function multisite_maintenance_form() {
  $sites = multisite_api_site_list();

  // create storage mechanism for all sites
  $form['sites'] = array('#type' => 'value', '#value' => array());

  // create storage mechanism for sites with unavailable DBs
  $form['unavailable_sites'] = array('#type' => 'value', '#value' => array());
    
  // set the timeout to be real low
  if ($timeout_error = !_multisite_maintenance_mysql_timeout(3)) {
    drupal_set_message('Unable to set the mysql timeout to a lower value.  This means that the multisite maintenance page might take as much as 2mins * (the number of sites) to load.');
  }

  // Traverse the sites retrieved and build the form.
  foreach ($sites as $site) {
    $dir = $site['dir'];
    $form['sites']['#value'][$dir]  = $site; 
    $form['dir'][$dir]              = array('#value' => $site['dir']);
    
    // obfuscate passwords
    $db_url = $site['db_url'];
    $db_default_dsn = $site['db_default_dsn'];
    $pass_star = str_repeat('*', strlen($db_default_dsn['pass']));
    $db_url = str_replace($db_default_dsn['pass'], $pass_star, $db_url);
    $db_default_dsn['pass'] = str_repeat('*', strlen($db_default_dsn['pass']));
    
    // db information
    $form['db_url'][$dir]           = array('#value' => $db_url);
    $form['db_prefix'][$dir]        = array('#value' => print_r($site['db_prefix'], true));
    $form['db_default_dsn'][$dir]   = array('#value' => print_r($db_default_dsn, true));
    
    // check if the DB can be accessed
    if (!$available = _multisite_maintenance_dsn_check($site['db_default_dsn'])) {
      $form['unavailable_sites']['#value'][$dir] = true;
      $form['db_reachable'][$dir] = array('#value' => 'No'); 
    }
    else {
      $form['db_reachable'][$dir] = array('#value' => 'Yes');       
    }
  } 

  // reset timeout value
  if (!$timeout_error) {
    _multisite_maintenance_mysql_timeout();
  }
    
  return $form;
}


/**
 * multisite status page
 */
function multisite_maintenance_status_form() {
  $sites = multisite_api_site_list();
  $form = multisite_maintenance_form();
  
  $form['status'] = array('#tree' => true);

  // Traverse the sites retrieved and build the form.
  foreach ($sites as $site) {
    $dir = $site['dir'];

    // site status
    $form['status']['status.'. $dir] = array(
      '#type'           => 'radios',
      '#options'        => array('Off-line', 'Online'),
      '#process'        => array(
        'expand_radios' => array(),
      ),
    );
    
    // check if the DB can be accessed
    if ($form['unavailable_sites']['#value'][$dir]) {
      $form['status']['status.'. $dir]['#attributes']['disabled']  = 'disabled';
    }
    else {
      // find out the current state of the site and adjust the radio accordingly
      $status = _multisite_maintenance_is_site_online($site);
      $form['sites']['#value'][$dir]['site_status']     = (int)$status;
      $form['status']['status.'. $dir]['#default_value']           = (int)$status;
    }
  } 
    
  // session clear
  $form['clear_sessions'] = array(
    '#type'           => 'checkbox',
    '#default_value'  => true, 
    '#title'          => t('Clear sessions tables'),
    '#description'    => t('Clearing sessions will log out any users currently logged in.  If this table is shared accross sites (likely situation) you will log users out of all sites.  You will not logged out because your session is written back to the table after it is truncated.  '),
  );

  // submit button
  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
  );

  return $form;
}

function multisite_maintenance_status_form_submit($form_id, $form_values) {

  // clear any messages (such as "You are operating in offline mode", cause chances are we won't when we're done)
  unset($_SESSION['messages']);

  // update site status
  foreach ($form_values['sites'] as $site => $site_info) {
    if ($form_values['unavailable_sites'][$site] || $form_values['status']['status.'. $site] == $site_info['site_status']) {
      continue;
    }
      
    $error = null;

    // connect to the db, this time we can use drupal query functions
    _multisite_maintenance_connect_to_db($site_info);
          
    if (!count($error)) {
      
      // update the variable
      variable_set('site_offline', !$form_values['status']['status.'. $site]);
      
      // truncate sessions if nec.
      if ($form_values['clear_sessions'] && !db_query('TRUNCATE sessions')) {
        drupal_set_message('sessions error', 'error');
        $error[] = true;
      }

      // clear the cache
      cache_clear_all('*', 'cache', TRUE);
      cache_clear_all('*', 'cache_page', TRUE);
      cache_clear_all('*', 'cache_menu', TRUE);
      cache_clear_all('*', 'cache_filter', TRUE);
      drupal_clear_css_cache();
    }
    else {
      drupal_set_message('connection error', 'error');
    }
    
    // close the db
    _multisite_maintenance_connect_to_db();
    
    // give some feedback
    $msg = $site .(count($error) ? ' could not be put ' : ' was taken ');
    $msg .= ($form_values['status']['status.'. $site] ? 'on' : 'off-') .'line.';
    $msg .= count($error) ? 'You\'ll need to do it manually.' : '';
    drupal_set_message($msg, count($error) ? 'error' : 'status');
  }

}

/**
 * multisite update page
 */
function multisite_maintenance_update_form() {
  $sites = multisite_api_site_list();
  $form = multisite_maintenance_form();

  $form['update'] = array('#tree' => true);

  // Traverse the sites retrieved and build the form.
  foreach ($sites as $site) {
    $dir = $site['dir'];

    // site update
    $form['update']['update.'. $dir] = array(
      '#type'           => 'checkbox',
    );
    
    // check if the DB can be accessed
    if ($form['unavailable_sites']['#value'][$dir]) {
      $form['update']['update.'. $dir]['#attributes']['disabled'] = 'disabled';
    }
    else {
     $form['update']['update.'.$dir]['#default_value'] = true;      
    }
  } 
    
  // backup
  if (_multisite_maintenance_test_shell_app('mysqldump') && file_check_directory(variable_get('multisite_maintenance_backup_dir', ''))) {
    $file = 'host_dbname_yyyy_mm_dd.mysql';
    if (_multisite_maintenance_test_shell_app('gzip')) {
      $file .= '.gz';
    }
    $form['backup'] = array(
      '#type'           => 'checkbox',
      '#default_value'  => true, 
      '#title'          => t('Backup database(s) before updating.'),
      '#description'    => t('Backups of the database(s) will be stored in %files in the form %format.', array('%files' => variable_get('multisite_maintenance_backup_dir', ''), '%format' => 'host_dbname_yyyy_mm_dd.mysql.gz')),
    );
  }
  else {
    $form['backup'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Backup database(s) before updating.'),
      '#attributes'     => array('disabled' => 'disabled'),
      '#description'    => t('Your database(s) can not be backed up.  See the info tab for more information'),
    );    
  }

  // submit button
  $form['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
  );

  return $form;
}

function multisite_maintenance_update_form_submit($form_id, $form_values) {

  // storage mechanism for backed up sites
  $backed_up = array();
  
  // switch to user1 so we pass the access check
  global $user;
  $old_user = $user;
  $user = user_load(array('uid' =>1));
  
  // update sites
  foreach ($form_values['sites'] as $site => $site_info) {
    if ($form_values['unavailable_sites'][$site] || !$form_values['update']['update.'.$site]) {
      continue;
    }

    // give each site 5 mins to do its thing
    set_time_limit(300);
          
    $error = null;
    
    // backup
    if ($form_values['backup'] && _multisite_maintenance_test_shell_app('mysqldump') && file_check_directory(variable_get('multisite_maintenance_backup_dir', ''))) {
      
      // db_url
      $db_key = $site_info['db_dsn']['host'] . $site_info['db_dsn']['path'];
      if (!array_key_exists($db_key, $backed_up)) {
        $ret = check_plain(_multisite_maintenance_backup_db($site_info['db_dsn']));
        if (strlen($ret)) {
          $error[] = $ret;
          watchdog('multisite_maintenance', $ret);
          drupal_set_message($ret .'!', 'error');
        }
        else {
          drupal_set_message($db_key .' has been backed up');
          $backed_up[$db_key] = true;
        }
      }
      
      // db_default_url
      $db_key = $site_info['db_default_dsn']['host'] . $site_info['db_default_dsn']['path'];
      if (!array_key_exists($db_key, $backed_up)) {
        $ret = check_plain(_multisite_maintenance_backup_db($site_info['db_default_dsn']));
        if (strlen($ret)) {
          $error[] = $ret;
          watchdog('multisite_maintenance', $ret);
          drupal_set_message($ret .'!', 'error');
        }
        else {
          drupal_set_message($db_key .' has been backed up');
          $backed_up[$db_key] = true;
        }
      }
    }
          
    //run the update
    if (!count($error)) {

      // switch to the other db
      _multisite_maintenance_connect_to_db($site_info);
      
      // reset the page timer
      timer_start('page');
      
      // get the list of updated versions
      include_once('update.php');
      $update_form = update_script_selection_form();
      $_POST['start'] = array();
      foreach ($update_form['start'] as $name => $module) {
        if (substr($name, 0, 1) == '#') {
          continue;
        }
        $_POST['start'][$name] = $module['#default_value']; 
      }
      
      /**
       * the below code is stolen from update.php update_update_page()
       */ 
      // Set the installed version so updates start at the correct place.
      foreach ($_POST['start'] as $module => $version) {
        drupal_set_installed_schema_version($module, $version - 1);
        $updates = drupal_get_schema_versions($module);
        $max_version = max($updates);
        if ($version <= $max_version) {
          foreach ($updates as $update) {
            if ($update >= $version) {
              $_SESSION['update_remaining'][] = array('module' => $module, 'version' => $update);
            }
          }
        }
      }
      // Keep track of total number of updates
      if (isset($_SESSION['update_remaining'])) {
        $_SESSION['update_total'] = count($_SESSION['update_remaining']);
      }
           
      /**
       * the below code is stolen from update.php update_do_updates() and slightly modified
       */ 
      while (isset($_SESSION['update_remaining']) && ($update = reset($_SESSION['update_remaining']))) {
        $update_finished = update_data($update['module'], $update['version']);
        if ($update_finished == 1) {
          // Dequeue the completed update.
          unset($_SESSION['update_remaining'][key($_SESSION['update_remaining'])]);
          $update_finished = 0; // Make sure this step isn't counted double
        }
        /** this has been lengthened so that each site update runs for 1 min before we give up **/
        if (timer_read('page') > 1000 *3600) {
          break;
        }
      }
      if ($_SESSION['update_total']) {
        $percentage = floor(($_SESSION['update_total'] - count($_SESSION['update_remaining']) + $update_finished) / $_SESSION['update_total'] * 100);
      }
      else {
        $percentage = 100;
      }

      // When no updates remain, clear the caches in case the data has been updated.
      if (!isset($update['module'])) {
        cache_clear_all('*', 'cache', TRUE);
        cache_clear_all('*', 'cache_page', TRUE);
        cache_clear_all('*', 'cache_menu', TRUE);
        cache_clear_all('*', 'cache_filter', TRUE);
        drupal_clear_css_cache();
      }

      // close the db
      _multisite_maintenance_connect_to_db();
    }
    else {
      drupal_set_message('backup error', 'error');
    }    
    
    // give some feedback
    if (count($error)) {
      $msg = $site .' could not be updated.  You\'ll need to do it manually.';    
    }
    else {
      $msg = $percentage .'% of '. $site .' was updated.  ';
    }
    drupal_set_message($msg, count($error) ? 'error' : 'status');
  }
  
  // set user back
  $user = $old_user;
}

/**
 * @subpackage THEME FUNCTIONS
 */
function theme_multisite_maintenance_status_form($form) {

  // Individual table headers.
  $header = array(t('Site Online'), t('Sites Directory'), t('db_url'), t('db_prefix'), t('Default DSN'), t('DB is Reachable'));
  
  // build table
  $rows = array();
  foreach($form['sites']['#value'] as $key => $site) {
    if (substr($key, 0,1) == '#') {
      continue;
    }
    $row = array();
    $row[] = drupal_render($form['status']['status.'. $key]);
    $row[] = '<strong>'. drupal_render($form['dir'][$key]) .'</strong>';
    $row[] = drupal_render($form['db_url'][$key]);
    $row[] = '<pre>'. drupal_render($form['db_prefix'][$key]) .'</pre>';
    $row[] = '<pre>'. drupal_render($form['db_default_dsn'][$key]) .'</pre>';
    $row[] = '<strong>'. drupal_render($form['db_reachable'][$key]) .'</strong>';
    $rows[] = $row;
  }
  $output .= theme('table', $header, $rows, array('class' => 'package'));
  
  // render remaining form objects and return
  $output .= drupal_render($form);
  return $output;
}

function theme_multisite_maintenance_update_form($form) {

  // Individual table headers.
  $header = array(t('Update Site'), t('Sites Directory'), t('db_url'), t('db_prefix'), t('Default DSN'), t('DB is Reachable'));
  
  // build table
  $rows = array();
  foreach($form['sites']['#value'] as $key => $site) {
    if (substr($key, 0,1) == '#') {
      continue;
    }
    $row = array();
    $row[] = drupal_render($form['update']['update.'. $key]);
    $row[] = '<strong>'. drupal_render($form['dir'][$key]) .'</strong>';
    $row[] = drupal_render($form['db_url'][$key]);
    $row[] = '<pre>'. drupal_render($form['db_prefix'][$key]) .'</pre>';
    $row[] = '<pre>'. drupal_render($form['db_default_dsn'][$key]) .'</pre>';
    $row[] = '<strong>'. drupal_render($form['db_reachable'][$key]) .'</strong>';
    $rows[] = $row;
  }
  $output .= theme('table', $header, $rows, array('class' => 'package'));
  
  // render remaining form objects and return
  $output .= drupal_render($form);
  return $output;
}


/**
 * @subpackage INTERNAL_FUNCTIONS
 */

/**
 * Checks a DSN array to see if connection to the db can be made
 * @param array $dsn
 *  an associative array of the db connection info in the form returned by parse_url()
 * @return boolean 
 *  true on success else false
 * @todo postgres
 *  unfortunately we can't use drupal db functions here.  PostgreSQL users will need to patch this
 */
function _multisite_maintenance_dsn_check($dsn) {

  // test db connection
  $link = @mysql_connect($dsn['host'] . ($dsn['port'] ? ':'. $dsn['port'] : ''), $dsn['user'], $dsn ['pass'], true) 
    or $error = true;
  if (empty($link)) {
    $error = true;
  }
  if (!$error) {
    $db = substr($dsn['path'], 1);
    $link_db = @mysql_select_db($db, $link)
      or $error = true;
    if (!$link_db) {
      $error = true;
    }
    @mysql_close($link);
  }

  
  return !$error;
}


/**
 * adjusts mysql.connect_timeout
 * pass an int to set it, leave arg blank to reset it
 */
function _multisite_maintenance_mysql_timeout($new_value = null) {
  static $old_timeout;
  if (!is_null($new_value)) {
    
    // store the old value
    if (is_null($old_timeout)) {
      if (!($old_timeout = ini_get('mysql.connect_timeout'))) {
        $error = true;
      }
      
      // set the new value
      if (!$error && ini_set('mysql.connect_timeout', $new_value)) {
        return true;
      }
      return false;        
    }
    else {
      // whoops! we tried to set the timeout a second time without reseting
      return false;
    }
  }
  else {
    
    // reset the timer
    // check for saved value
    if (!is_null($old_timeout) && ini_set('mysql.connect_timeout', $old_timeout)) {
      $old_timeout = null;
      return true;
    }
    return false;        
  }
}


/**
 * -adjusts $_GLOBALS['db_url'] and $_GLOBALS['db_prefix']
 * -mounts the db
 * -adjusts all other globals (if possible)
 * -clear cache
 * @param array
 *  $site in the form as created by multisite_api_site_list()
 *  or leave null to reset to the original values
 */
function _multisite_maintenance_connect_to_db($site = null) {

  global $db_url, $db_prefix, $base_url, $base_root, $base_path;
  
  static $original_site;
  
  if ($site) {
    
    // store the old value
    if (!isset($original_site)) {
      $sites = multisite_api_site_list();
      $original_site = $sites[substr(conf_path(), 6)];
    }
    
    // set the new value
    if (is_array($db_url)) {
      $db_url[$site['dir']] = $site['db_url'];
    }
    else {
      $db_url = array('default' => $db_url);
      $db_url[$site['dir']] = $site['db_url'];     
    }
    $db_prefix = $site['db_prefix'];
    if ($site['base_url']) {
      $base_url = $site['base_url'];      
    }
    else {
      unset($base_url); 
    }
    db_set_active($site['dir']);

  }
  else {
    
    // reset the db stuff
    // check for saved value
    if (isset($original_site)) {
      $db_url = $original_site['db_url'];
      $db_prefix = $original_site['db_prefix'];
      if ($original_site['base_url']) {
        $base_url = $original_site['base_url'];      
      }
      else {
        unset($base_url); 
      }
      db_set_active();
      $site = $original_site;
      unset($original_site);
    }
    else {
      drupal_set_message('whoops! we tried to reset before we set', 'error');
      return false;
    }        
  }

  // clear our cache for this site
  cache_clear_all('*', 'cache', TRUE);
  cache_clear_all('*', 'cache_page', TRUE);
  cache_clear_all('*', 'cache_menu', TRUE);
  cache_clear_all('*', 'cache_filter', TRUE);
  drupal_clear_css_cache();
    
  /** rebuild all important globals **/
  // $base_url, $base_path, $base_root
  // this code grabbed from bootsrtap.inc conf_init 
  if (isset($base_url)) {
    // Parse fixed base URL from settings.php.
    $parts = parse_url($base_url);
    if (!isset($parts['path'])) {
      $parts['path'] = '';
    }
    $base_path = $parts['path'] . '/';
    // Build $base_root (everything until first slash after "scheme://").
    $base_root = substr($base_url, 0, strlen($base_url) - strlen($parts['path']));
  }
  else {
    // if $base_url is not passed to us, we cannot reverse engineer the others, 
    // unset them so that an error is thrown if they are used
    unset ($base_root, $base_path);
  }
  
  // variables
  cache_clear_all('*', 'cache', TRUE);
  
  global $debug;
  $debug = true;
  unset($GLOBALS['conf']);
  $GLOBALS['conf'] = variable_init((array)$site['conf']);

}

/**
 * switches to the given site and asks whether it's online
 */
function _multisite_maintenance_is_site_online($site) {

  // switch to the given site
  _multisite_maintenance_connect_to_db($site);

  // are we online?
  $offline = variable_get('site_offline', 'default'); 
//  drupal_set_message('$offline:'. $offline);
  
  // switch back
  _multisite_maintenance_connect_to_db();
  
  return !$offline;
}

/**
 * tests if a shell application exists
 * @param string $app
 *  either 'gzip' or 'mysqldump'
 * @return boolean 
 *  true if it exists, else false
 */
function _multisite_maintenance_test_shell_app($app) {
  static $gzip;
  static $mysqldump;
  
  if ($$app) {
    return $app;
  }
  switch ($app) {
    case 'gzip':
      $info = shell_exec('gzip -L');
      $gzip = strpos($info, 'WARRANTY') !== false;
      return $gzip;
    break;
    case 'mysqldump':
      $info = shell_exec('mysqldump -V');
      $mysqldump = strpos($info, 'Ver') !== false;
      return $mysqldump;
    break;
  }
}

/**
 * backs up the given db.  
 * @param array 
 *  $dsn must be in the form as returned by parse_url()
 * @return string
 *  the output of the command
 */
function _multisite_maintenance_backup_db($dsn) {
  $db = substr($dsn['path'], 1);
  $gzip = _multisite_maintenance_test_shell_app('gzip');
  
  // build filename
  $ext = '.mysql'. ($gzip ? '.gz' : '');
  $file = variable_get('multisite_maintenance_backup_dir', '') .'/'. $dsn['host'] .'_'. $db .'_'. date('Y_m_d');
  $inc = '';
  $x = 1;
  
  // test if exists
  while (file_exists($file_name =($file . $inc . $ext))) {
    $inc = "__$x";
    $x ++;
  }

  // build command    
  $cmd = 'mysqldump -u'. $dsn['user'] .' -p'. $dsn['pass'] .' -h'. $dsn['host'] .' '. $db;
  if ($gzip) {
    $cmd .= ' | gzip';
  }
  $cmd .= ' > '. $file_name;

  // execute command
  $return = shell_exec($cmd);
  return $return;  
}